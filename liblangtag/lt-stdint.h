#ifndef _LIBLANGTAG_LIBLANGTAG_LT_STDINT_H
#define _LIBLANGTAG_LIBLANGTAG_LT_STDINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "liblangtag 0.6.4"
/* generated using gnu compiler gcc (GCC) 12.1.1 20220507 (Red Hat 12.1.1-1) */
#define _STDINT_HAVE_STDINT_H 1
#include <stdint.h>
#endif
#endif
